<!--
SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>

SPDX-License-Identifier: MPL-2.0
-->

# aoc-next

Helper library for [Advent of Code](https://adventofcode.com/2020/about), inspired by [`cargo-aoc`](https://github.com/gobanos/cargo-aoc).

# Features

* Display the execution time of your parser and solving functions.
* Automatically download the inputs
* Filter solutions for a specific day
* First parse the input into a nicer struct, then process it in a solution

## To Do 

* Benchmarking?
* Better support of generics in the return types of the parser functions

# Usage

## General

Include the crate as a library. 
Then declare a static `Aoc` struct to 'register' your solutions.
It will probably look similiar to this one:
```rust
const AOC: Aoc = Aoc {
    allow_download: true,
    year: 2020,
    solutions: &[
        solution!{1, failable_parser!{ day1::input_generator }, solver!{ day1::solve_part1 }},
        solution!{1, failable_parser!{ day1::input_generator }, solver!{ day1::solve_part2 }},
        solution!{2, failable_parser!{ day2::input_generator }, solver!{ day2::solve_part1 }},
        solution!{2, failable_parser!{ day2::input_generator }, solver!{ day2::solve_part2 }},
    ],
};
```
To be able to run your solutions add `app::aoc_main` to your main method.
It will take care of providing the input to your parsers and executing your solutions.

If you only want to see the solutions for a specific day, just pass the 
number as the first argument: `cargo run --release 2` to run all solutions for day 2, for example.

## Automatic input download

You will need to find your session token for the AoC in order for the automatic download to work. 
Thankfully, finding your token is easy since it is stored in your Browser's cookies.
Open up the devtools of your browser, and then :

* Firefox: "Storage" tab, Cookies, and copy the "Value" field of the session cookie.
* Google Chrome / Chromium: "Application" tab, Cookies, and copy the "Value" field of the session cookie.

Now put them into `.aoc_session.txt` next to your project's `Cargo.toml`. 

*Important*: Make sure to add `.aoc_session.txt` to your `.gitignore`, so you don't actually commit 
your session token!

## Example(s)

See https://github.com/liketechnik/aoc-2020 for example usage of the library or open an issue if you should have any problems.
I'm also available on the rust discord(s) (as liketechnik#8562).
