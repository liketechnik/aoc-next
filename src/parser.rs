// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use anyhow::Result;

/// Container for a parsing function.
///
/// The contained function transforms the input file into
/// a better usable structure.
///
/// Allows to have both failable and infailable parsers,
/// respective [`Parser`] and [`FailableParser`] (where the function type is a result).
pub trait Parsing<P> {
    /// Transforms the file input into a better usable structure.
    fn run(&self, input: &str) -> Result<P>;
    /// The name of the parsing function.
    fn name(&self) -> &'static str;
}

/// Container for an infailable parser.
pub struct Parser<F, P>
where
    F: Fn(&str) -> P,
{
    pub run: F,
    pub name: &'static str,
}

impl<F, P> Parsing<P> for Parser<F, P>
where
    F: Fn(&str) -> P,
{
    fn run(&self, input: &str) -> Result<P> {
        Ok((self.run)(input))
    }

    fn name(&self) -> &'static str {
        self.name
    }
}

/// Container for an failable parser.
pub struct FailableParser<F, P>
where
    F: Fn(&str) -> Result<P>,
{
    pub run: F,
    pub name: &'static str,
}

impl<F, P> Parsing<P> for FailableParser<F, P>
where
    F: Fn(&str) -> Result<P>,
{
    fn run(&self, input: &str) -> Result<P> {
        (self.run)(input)
    }

    fn name(&self) -> &'static str {
        self.name
    }
}
