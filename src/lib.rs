// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0
//! Helper library for [Advent of Code](https://adventofcode.com/2020/about).
//! Inspired by `cargo-aoc`.

pub mod app;
pub mod input;
pub mod macros;
pub mod parser;
pub mod solution;

pub use app::*;
pub use parser::{FailableParser, Parser};
pub use solution::{Solution, Solver};
