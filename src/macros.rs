// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0
//! Helper macros to ease the declaration of the functions that contain your solutions of the
//! exercises.
//!
//! * Use [`solution!()`] to declare a new solution inside of
//! [`crate::app::Aoc#structfield.solutions`].
//! * The library currently expects that you want to parse the input. Use either:
//!     * [`failable_parser!()`] if the parsing function returns a [`std::result::Result`];
//!     * [`parser!()`] if the parsing function returns the plain return value.
//! * Use [`solver!()`] to point to the function that solves the exercise.

/// Declare a new solution:
///
/// * `$day` is the day of the exercise this solution solves. This is used to download the input
/// file automatically.
/// * `$parser` is an instance of [`crate::parser::Parsing`]. (See [`failable_parser!()`] or
/// [`parser!()`])
/// * `$solver` is an instance of [`crate::solution::Solver`]. (See [`solver!()`])
#[macro_export]
macro_rules! solution {
    ($day:expr, $parser:expr, $solver:expr) => {{
        &$crate::Solution {
            day: $day,
            parser: $parser,
            solver: $solver,
        }
    }};
}

/// Declare a new failable parser.
///
/// The function has to take a `&`[`std::str`].
/// The return type must be a [`std::result::Result`],
/// though the inner types have no restriction, as long
/// as the [`std::result::Result::Ok`] type matches the parameter type of the solving function.
#[macro_export]
macro_rules! failable_parser {
    ($parser:expr) => {{
        $crate::FailableParser {
            run: $parser,
            name: stringify!($parser),
        }
    }};
}

/// Declare a new infailable parser.
///
/// The function has to take a `&`[`std::str`].
/// The return type has no restriction, as long
/// as it matches the parameter type of the solving function.
#[macro_export]
macro_rules! parser {
    ($parser:expr) => {{
        $crate::Parser {
            run: $parser,
            name: stringify!($parser),
        }
    }};
}

/// Declare a new solver, e. g. a funtion that solves an exercise.
///
/// The function's parameter type has to match the return value of
/// the parser used for the [`crate::solution::Solution`].
/// The return value must implement [`std::fmt::Display`],
/// so the result can be printed to stdout.
#[macro_export]
macro_rules! solver {
    ($solver:expr) => {{
        $crate::Solver {
            marker: ::std::marker::PhantomData,
            name: stringify!($solver),
            run: $solver,
        }
    }};
}
