// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0
//! Helpers for executing solutions.
//!
//! See [`aoc_main()`] for an easy to use entrypoint.
//!
//! The implementations to execute can be
//! defined in the [`Aoc`] struct.
//! The different macros in this crate can ease the duty of
//! defining each implementation.

use anyhow::{anyhow, Result};

use crate::solution::Solvable;

/// Central configuration for the [`aoc_main()`] entrypoint.
pub struct Aoc {
    /// Set to true to automatically download the input files.
    pub allow_download: bool,
    /// The year of AoC the exercises are from.
    pub year: u64,
    /// Your solutions of the exercises.
    pub solutions: &'static [&'static dyn Solvable],
}

/// Execute all exercise solutions defined
/// in an [`Aoc`] struct.
pub fn aoc_main(aoc: Aoc) -> Result<()> {
    println!("AOC {}", aoc.year);

    let mut day: Option<u8> = None;
    if let Some(arg) = std::env::args().nth(1) {
        if let Ok(d) = arg.parse() {
            day = Some(d);
        } else {
            return Err(anyhow!("Specify a number to only run the solutions for that specific day. Otherwise all solutions are executed."));
        }
    }

    for solution in aoc.solutions {
        if let Some(day) = day {
            if solution.day() != day {
                continue;
            }
        }
        solution.run(&aoc)?
    }

    Ok(())
}
